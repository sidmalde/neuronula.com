<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Neuro Nula :: Welcome to our landing page!</title>
	<meta name="description" content="Neuro Nula :: Welcome to our landing page!">
	<meta name="author" content="SitePoint">
	<link rel="stylesheet" href="css/core.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<div class="container">
		<div class="well">
			<div class="jumbotron">
				<h1 class="text-center">Neuro Nula</h1>
				<br />
				<div class="">
					<p class="text-success text-center">
						An online community for people with neurological conditions, their families and carers. 
						<br />
						Coming soon...
					</p>
					
					<h3 class="text-muted">About us:</h3>
					<p class="text-success">
						Neuro Nula aims to bring people living, or caring for someone, with a neurological 
						condition together to share their experiences, advice and make new friends 
						in a place free from judgement and stigmatisation.
					</p>
					<p class="text-success">
						Neuro Nula will provide a safe, moderated forum, links to neurological organisations 
						and access to the stories of people affected by a range of neurological conditions. If 
						you would like to share your story, contact NeuroNula@live.com
					</p>
				</div>
			</div>
			<div>
				
			</div>
			<div class="row">
				<div class="col-4">
					<a href="https://twitter.com/NeuroNula" target="_blank">
						<div class="img-social-twitter" style="float: none; margin: 0 auto;">
						&nbsp;
						</div>
					</a>
				</div>
				<div class="col-4">	
					<a href="http://www.facebook.com/NeuroNulaUK" target="_blank">
						<div class="img-social-fb" style="float: none; margin: 0 auto;">
							&nbsp;
						</div>
					</a>
				</div>
				<div class="col-4">
					<a href="mailto:NeuroNula@live.com">
						<div class="img-social-mail" style="float: none; margin: 0 auto;">
							&nbsp;
						</div>
					</a>
				</div>
			</div>
			<?/*<div class="img-container">
				<a href="https://twitter.com/NeuroNula">
					<div class="img-social-twitter">
						&nbsp;
					</div>
				</a>
				<a href="https://twitter.com/NeuroNula">
					<div class="img-social-fb">
						&nbsp;
					</div>
				</a>
				<a href="mailto:NeuroNula@live.com">
					<div class="img-social-mail">
						&nbsp;
					</div>
				</a>
			</div>*/?>
		</div>
	</div>
</body>
</html>